php >= 8.0

# 引入

```
composer require wanjing/utility
```

# 示例
### 数组相关

```
use Wanjing\Utility\Arr;

// 获取数组的树形结构
$arr = [
    ["id" => 1,"name" => "张三","parentId" => 0],
    ["id" => 2,"name" => "李四","parentId" => 1],
    ["id" => 3,"name" => "王五","parentId" => 2],
];
var_dump(Arr::tree($arr,'id','parentId','children'));
输出： 
["id" => 1 ,"name" => "张三","parentId" => 0,"children" => [["id" => 2,"name" => "李四","parentId" => 1,"children" => [["id" => 3,"name" => "王五","parentId" => 2]]]]]

// 将数组转换为以某个字段为键的数组
$arr = [
    ["id" => 1,"name" => "张三"],
    ["id" => 2,"name" => "李四"],
    ["id" => 3,"name" => "王五"],
];
var_dump(Arr::indexBy($arr,'id'));
输出：
[1 => ["id" => 1,"name" => "张三"],2 => ["id" => 2,"name" => "李四"],3 => ["id" => 3,"name" => "王五"]]
```
### 字符串相关
```
use Wanjing\Utility\Str;

// 获取随机订单号
var_dump(Str::orderNo('T',18)); // T20201223100000001

// 判断字符串是否包含emoji字符
var_dump(Str::hasSpecialChar('哈哈🙂')); // true

// 生成随机密码
var_dump(Str::makePassword(8)); // D4dqwS5d

// 驼峰转下划线
var_dump(Str::snake('userName')); // user_name

// 下划线转驼峰
var_dump(Str::camelize('user_name')); // userName
```
### 日期
```
use Wanjing\Utility\Date;

// 日期语义化
var_dump(Date::humanDate('2023-05-01')); // 1周前

// 获取一年多少天
var_dump(Date::calDaysInYear('2023')); // 365

// 获取指定日期是周几
var_dump(Date::week()); // 周一
```