<?php

namespace Wanjing\Utility;

/**
 * 处理时间相关
 */
class Date
{

    /**
     * 时间语义化转换
     * @param string|int $date 日期
     * @param string|int $current 当前日期
     * @return string
     */
    static function humanDate(string|int $date,string|int $current = 0): string
    {
        if (is_string($current)) $current = strtotime($current);
        if (is_string($date)) $date = strtotime($date);

        if (!$current || !is_numeric($current)) $current = time();
        if (!$date || !is_numeric($date)) $date = time();

        $suffix = '前';
        $seconds = $current - $date;

        if ($seconds < 0) {
            $suffix = '后';
            $seconds = abs($seconds);
        }

        $data = match (true) {
            $seconds >= 31536000 => bcdiv($seconds , 31536000) . '年',
            $seconds >= 2592000 => bcdiv($seconds , 2592000) . '月',
            $seconds >= 604800 => bcdiv($seconds , 604800) . '周',
            $seconds >= 86400 => bcdiv($seconds , 86400) . '天',
            $seconds >= 3600 => bcdiv($seconds , 3600) . '小时',
            $seconds >= 60 => bcdiv($seconds , 60) . '分钟',
            default => $seconds . '秒',
        };

        return $data.$suffix;
    }

    /**
     * 获取一年多少天
     * @param int|null $year
     * @return int
     */
    static function calDaysInYear(?int $year): int
    {
        is_null($year) && $year = date('Y');
        // 获取本年度时间范围
        $yearStart = strtotime($year . "-01-01 00:00:00");
        $nextYearStart = strtotime(($year + 1) . "-01-01 00:00:00");

        // 计算天数
        return bcdiv(($nextYearStart - $yearStart) , (60 * 60 * 24));
    }

    /**
     * 获取指定日期是星期几.
     * @param string $date 日期
     * @param int $type 1=数字 0=中文
     * @return int|string
     */
    static function week(string $date, int $type = 0): int|string
    {
        $weekDays = array("周日", "周一", "周二", "周三", "周四", "周五", "周六");

        $num = date("w", strtotime($date));
        if ($type) {
            return $num;
        }
        return $weekDays[$num];
    }
}
