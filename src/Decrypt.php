<?php

namespace Wanjing\Utility;

/**
 * 加密解密相关
 */
class Decrypt
{
    /**
     * rsa公钥加密
     * @param string $str 需要加密内容
     * @param string $key 公钥
     * @return string|null
     */
    static function rsaEncrypt(string $str,string $key = ''): ?string
    {
        empty($key) && $key = config('rsa.public_key');
        $puKey = openssl_pkey_get_public($key);//这个函数可用来判断公钥是否是可用的
        openssl_public_encrypt($str, $encryptData, $puKey);
        return base64_encode($encryptData);
    }

    /**
     * rsa私钥解密
     * @param string $str 需要解密内容
     * @param string $key 私钥
     * @return string|null
     */
    static function rsaDecrypt(string $str,string $key = ''): ?string
    {
        empty($key) && $key = config('rsa.private_key');
        $piKey =  openssl_pkey_get_private($key);//这个函数可用来判断私钥是否是可用的，可用返回资源id Resource id
        openssl_private_decrypt(base64_decode($str), $decryptData, $piKey);
        return $decryptData;
    }
}